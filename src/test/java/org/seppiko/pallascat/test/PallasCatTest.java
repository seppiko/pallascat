/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pallascat.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import org.junit.jupiter.api.Test;

/**
 * @author Leonard Woo
 */
public class PallasCatTest {

  @Test
  public void test() throws Exception {
    // TODO:
    String url = "jdbc:manul://localhost:3200/database?useUnicode=true";
    String username = "root";
    String password = "toor";
    String sql = "SELECT 1";
//    Class.forName("org.seppiko.pallascat.Driver");
    Connection conn = DriverManager.getConnection(url, username, password);
    Statement stat = conn.createStatement();
    ResultSet rs = stat.executeQuery(sql);
    ResultSetMetaData row = rs.getMetaData();
    while (rs.next()) {
      System.out.print("| ");
      for (int i = 0; i < row.getColumnCount(); i++) {
        System.out.print(row.getColumnName(i) + " " + rs.getString(i) + " | ");
      }
      System.out.println();
    }
  }

}
