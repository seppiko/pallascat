/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pallascat;

import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import org.seppiko.commons.jdbc.CommonDriver;

/**
 * @author Leonard Woo
 */
public final class Driver implements CommonDriver {

  private final String JDBC_URL = "jdbc:";
  private final String JDBC_PROTOCOL = "manul:";
  private static final Logger logger = Logger.getLogger("org.seppiko.pallascat.Driver");

  private static final List<DriverPropertyInfo> DRIVER_PROPERTIES = new ArrayList<>();

  static {


    try {
      DriverManager.registerDriver(new Driver());
    } catch (SQLException e) {
      throw new RuntimeException("Driver register failed", e);
    }
  }

  /**
   * Attempts to make a database connection to the given URL.
   * The driver should return "null" if it realizes it is the wrong kind
   * of driver to connect to the given URL.  This will be common, as when
   * the JDBC driver manager is asked to connect to a given URL it passes
   * the URL to each loaded driver in turn.
   *
   * <P>The driver should throw an <code>SQLException</code> if it is the right
   * driver to connect to the given URL but has trouble connecting to
   * the database.
   *
   * <P>The {@code Properties} argument can be used to pass
   * arbitrary string tag/value pairs as connection arguments.
   * Normally at least "user" and "password" properties should be
   * included in the {@code Properties} object.
   * <p>
   * <B>Note:</B> If a property is specified as part of the {@code url} and
   * is also specified in the {@code Properties} object, it is
   * implementation-defined as to which value will take precedence. For
   * maximum portability, an application should only specify a property once.
   *
   * @param url the URL of the database to which to connect
   * @param info a list of arbitrary string tag/value pairs as
   * connection arguments. Normally at least a "user" and
   * "password" property should be included.
   * @return a <code>Connection</code> object that represents a
   *         connection to the URL
   * @exception SQLException if a database access error occurs or the url is
   * {@code null}
   */
  @Override
  public java.sql.Connection connect(String url, Properties info) throws SQLException {
    if (!acceptsURL(url)) {
      throw new SQLException("Invalid URL: " + url);
    }

    Connection result = null;

    return result;
  }

  /**
   * Retrieves whether the driver thinks that it can open a connection
   * to the given URL.  Typically drivers will return <code>true</code> if they
   * understand the sub-protocol specified in the URL and <code>false</code> if
   * they do not.
   *
   * @param url the URL of the database
   * @return <code>true</code> if this driver understands the given URL;
   *         <code>false</code> otherwise
   * @exception SQLException if a database access error occurs or the url is
   * {@code null}
   */
  @Override
  public boolean acceptsURL(String url) throws SQLException {
    if (url == null || url.isBlank()) {
      return false;
    }
    return url.toLowerCase().startsWith(JDBC_URL + JDBC_PROTOCOL);
  }

  /**
   * Gets information about the possible properties for this driver.
   * <P>
   * The <code>getPropertyInfo</code> method is intended to allow a generic
   * GUI tool to discover what properties it should prompt
   * a human for in order to get
   * enough information to connect to a database.  Note that depending on
   * the values the human has supplied so far, additional values may become
   * necessary, so it may be necessary to iterate though several calls
   * to the <code>getPropertyInfo</code> method.
   *
   * @param url the URL of the database to which to connect
   * @param info a proposed list of tag/value pairs that will be sent on
   *          connect open
   * @return an array of <code>DriverPropertyInfo</code> objects describing
   *          possible properties.  This array may be an empty array if
   *          no properties are required.
   * @exception SQLException if a database access error occurs
   */
  @Override
  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
    acceptsURL(url);
    if (info == null || info.isEmpty()) {
      return new DriverPropertyInfo[0];
    }

    return DRIVER_PROPERTIES.toArray(new DriverPropertyInfo[0]);
  }

  /**
   * Retrieves the driver's major version number. Initially this should be 1.
   *
   * @return this driver's major version number
   */
  @Override
  public int getMajorVersion() {
    return 1;
  }

  /**
   * Gets the driver's minor version number. Initially this should be 0.
   * @return this driver's minor version number
   */
  @Override
  public int getMinorVersion() {
    return 0;
  }

  /**
   * Return the parent Logger of all the Loggers used by this driver. This
   * should be the Logger farthest from the root Logger that is
   * still an ancestor of all of the Loggers used by this driver. Configuring
   * this Logger will affect all of the log messages generated by the driver.
   * In the worst case, this may be the root Logger.
   *
   * @return the parent Logger for this driver
   * @throws SQLFeatureNotSupportedException if the driver does not use
   * {@code java.util.logging}.
   * @since 1.7
   */
  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return logger;
  }

}
