/**
 * java module
 */
module seppiko.pallascat {
  requires java.sql;
  requires seppiko.commons.jdbc;

  exports org.seppiko.pallascat;
}